/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.app.panels;

import biz.sos.exec.JmsBrowser;
import biz.sos.swing.util.BrowseTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JTable;

/**
 *
 * @author Daniel Fdez
 */
public class BrowseTable {

    public static JTable getTable(String host, int port, String queueManagerName, String channel, String queue) {
        
        JTable table = null;
        List<String> headers = new ArrayList<String>();
        headers.add("");
        headers.add("Date");
        headers.add("Body");
        try{
            JmsBrowser j = new JmsBrowser(host, port, queueManagerName, channel, queue);
            List<Map<String, String>> l = j.browse();
            
            BrowseTableModel tm = new BrowseTableModel(l);
            
            table = new JTable(tm);
            
            table.getColumnModel().getColumn(0).setPreferredWidth(70);
            table.getColumnModel().getColumn(1).setPreferredWidth(100);
            table.getColumnModel().getColumn(2).setPreferredWidth(400);
            
            table.setRowSelectionAllowed(true);
            table.setColumnSelectionAllowed(false);
            
        }catch(Exception e){
            System.out.println("BrowseTable... " + e);
            e.printStackTrace();
            String[][] data = new String[1][3];
            table = new JTable(data, headers.toArray());
        }
        return table;

    }
}
