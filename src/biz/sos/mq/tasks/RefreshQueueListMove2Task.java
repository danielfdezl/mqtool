/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.mq.app.panels.MovePanels;
import biz.sos.mq.pcf.WorkerPCF;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class RefreshQueueListMove2Task extends SwingWorker<Void, Void> {

    private MovePanels panel;

    @Override
    protected Void doInBackground() throws Exception {

        String filter = panel.getjTextField6().getText();
        JCheckBox checkFilter = panel.getjCheckBox2();
        JButton button = panel.getjButton4();

        panel.getjProgressBar1().setIndeterminate(true);
        button.setEnabled(false);
        panel.setWorking2(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        WorkerPCF pcf = null;
        if (panel.getPcf2() != null) {
            pcf = panel.getPcf2();
        } else {
            System.out.println("Conectese first...");
            return null;
        }

        if ("filter...".equals(filter.trim()) || "".equals(filter.trim())) {
            filter = "*";
        }

        List<String> names = pcf.getListQueueDepthOrdered(filter);

        DefaultListModel modelo = new DefaultListModel();
        for (String name : names) {
            if (panel.getjCheckBox2().isSelected() && name.startsWith("SYSTEM")) {
                continue;
            } else {
                if(panel.getjCheckBox2().isSelected() && !name.startsWith("AMQ.")){
                   modelo.addElement(name);
                }else if(!panel.getjCheckBox2().isSelected()){
                   modelo.addElement(name); 
                }
            }
        }
        panel.getjList1().setModel(modelo);
        return null;
    }

    public void done() {
        try {
            panel.getjProgressBar1().setIndeterminate(false);
            panel.getjButton4().setEnabled(true);
            panel.setWorking2(false);
            panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            Toolkit.getDefaultToolkit().beep();
        } catch (Exception e) {
            System.out.println("2RefreshQueueListMove1Task..." + e);
            e.printStackTrace();
        }
    }

    public void setPanel(MovePanels panel) {
        this.panel = panel;
    }
}
