/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.mq.app.panels.MovePanels;
import biz.sos.mq.pcf.WorkerPCF;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class RefreshQueueListMove1Task extends SwingWorker<Void, Void> {

    private MovePanels panel;

    @Override
    protected Void doInBackground() throws Exception {
        
        String filter = panel.getjTextField5().getText();
        JComboBox queue = panel.getjComboBox4();
        JCheckBox checkFilter = panel.getjCheckBox1();
        JButton button =   panel.getjButton3();
        
        panel.getjProgressBar1().setIndeterminate(true);
        button.setEnabled(false);
        panel.setWorking1(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      
        try {
            
            WorkerPCF pcf = null;
            if(panel.getPcf1() != null){
                pcf = panel.getPcf1();
            }else{
                System.out.println("Conectese first...");
                return null;
            }
            
            if ("filter...".equals(filter.trim()) || "".equals(filter.trim())) {
                filter = "*";
            }
  
            List<String> names = pcf.getListQueueDepth(filter);
            
            String selectedQueue1 = (String) queue.getSelectedItem();  
            queue.removeAllItems();

            int index = 0;
            int indexQueue = 0;
            for (String name : names) {
                if (checkFilter.isSelected() && name.startsWith("SYSTEM")) {
                    continue;
                } else {
                    
                    if(checkFilter.isSelected() && !name.startsWith("AMQ.")){
                        queue.addItem(name);
                        if(name.startsWith(selectedQueue1.substring(0,selectedQueue1.indexOf("(")+1))){
                           indexQueue = index;
                        }
                        index++;
                    }
                }
            }
            queue.setSelectedIndex(indexQueue);
        } catch (Exception e) {
            System.out.println("1RefreshQueueListMove1Task..."  + e);
            e.printStackTrace();
        }
        return null;
    }

    public void done() {
        try{
            panel.getjProgressBar1().setIndeterminate(false);
            panel.getjButton3().setEnabled(true);
            panel.setWorking1(false);
            panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            Toolkit.getDefaultToolkit().beep();
        }catch(Exception e){
            System.out.println("2RefreshQueueListMove1Task..."  + e);
            e.printStackTrace();
        }
    }

    public void setPanel(MovePanels panel) {
        this.panel = panel;
    }

}