/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.exec.Writer;
import biz.sos.mq.app.panels.QEditorPanel;
import biz.sos.mq.pcf.WorkerPCF;
import biz.sos.swing.util.StringOperations;
import com.ibm.jms.JMSTextMessage;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class PutMessageTask extends SwingWorker<Void, Void> {
    
    private QEditorPanel panel;
    private JButton button;
    private Writer writer = null;
    
    private JTextField filtro;
    private JComboBox queue1;
    private JComboBox queue2;
    private JCheckBox checkFilter;
    private JTextField quantity;
    
    @Override
    protected Void doInBackground() throws Exception {
        panel.getjProgressBar1().setIndeterminate(true);
        button.setEnabled(false);
        panel.setWorking(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        String ip = panel.getIp();
        String port = panel.getPort();
        String qmName = panel.getQmName();
        String channel = panel.getChannelsComboBox().getSelectedItem().toString();
        String queue = panel.getjComboBox1().getSelectedItem().toString();
        queue = queue.substring(0, queue.indexOf("("));
        String msg = panel.getjTextArea3().getText();
        
        int quantityInt = 1;
        try{
            quantityInt = Integer.parseInt(quantity.getText());
        }catch(Exception e){
            
        }
        
        if (panel.getjCheckBox6().isSelected()) {
            msg = msg.replaceAll("\n", "\r\n");
        }
        
        int ccsid = 0;
        int encoding = 0;
        boolean cc = false;
        if(panel.getCcsid() != null && !"".equals(panel.getCcsid())
                    && panel.getEncoding() != null && !"".equals(panel.getEncoding()) ){
            cc = true;
            
              ccsid = Integer.parseInt(panel.getCcsid().trim());
              encoding = Integer.parseInt(panel.getEncoding().trim());
        }
        
        writer = new Writer(ip, Integer.parseInt(port), qmName, channel, queue);
        if (panel.getjCheckBox4().isSelected()) {
            writer.setUseRFH(true);
        }
        if(panel.getReplyToQueue() != null){
            writer.setReplayTo(panel.getReplyToQueue());
        }
        for(int i=0; i<quantityInt; i++){

            String msgCp = msg.replaceAll("\\{sec2\\}", StringOperations.alphanumericRandom(3));
            msgCp = msgCp.replaceAll("\\{sec3\\}", StringOperations.numericRandom(3));  

            JMSTextMessage message = (JMSTextMessage) writer.getSession().createTextMessage(msgCp);
            if(cc){
                try{ 
                     writer.writeMessage(message, ccsid, encoding);
                }catch(Exception e){
                    writer.writeMessage(message);
                }
            }else{
                writer.writeMessage(message);
            }
            System.out.println(i + " ===> msgId: " + message.getJMSMessageID());
            refreshQueueList();
        }
        writer.close();
        return null;
    }
    
    private void refreshQueueList(){
        
         WorkerPCF pcf = null;
            if(panel.getPcf() != null){
                pcf = panel.getPcf();
            }else{
                pcf = new WorkerPCF(panel.getChannelsComboBox().getSelectedItem().toString(), panel.getIp(), panel.getPort());
                panel.setPcf(pcf);
            }
            
            String filter = filtro.getText();
            if ("filter...".equals(filter.trim()) || "".equals(filter.trim())) {
                filter = "*";
            }
  
            List<String> names = pcf.getListQueueDepth(filter);
            String selectedCombo1 =  queue1.getSelectedItem().toString();
            selectedCombo1 = selectedCombo1.substring(0,selectedCombo1.indexOf("("));

            String selectedCombo2 = null;
            queue1.removeAllItems();
            queue1.addItem("Queues...");
            if (queue2 != null) {
                selectedCombo2 =  queue2.getSelectedItem().toString();
                selectedCombo2 = selectedCombo2.substring(0,selectedCombo2.indexOf("("));
                queue2.removeAllItems();
                queue2.addItem("Queues...");
            }
            int i = 1;
            int selectedIndex1 = 0;
            int selectedIndex2 = 0;
            
            for (String name : names) {
                if (checkFilter.isSelected() && name.startsWith("SYSTEM")) {
                    continue;
                } else {
                    queue1.addItem(name);
                    if (queue2 != null) {
                        queue2.addItem(name);
                    }
                }
                
                if(name.substring(0,name.indexOf("(")).equals(selectedCombo1)){ 
                    selectedIndex1 = i;
                }
                if(name.substring(0,name.indexOf("(")).equals(selectedCombo2)){
                    selectedIndex2 = i;
                }
                i++;
            }
            queue1.setSelectedIndex(selectedIndex1);
            if (queue2 != null){
                queue2.setSelectedIndex(selectedIndex2);
            }   
        
        
    }    
     public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        button.setEnabled(true);
        panel.setWorking(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }
     
    public void setButton(JButton button) {
        this.button = button;
    } 

    public void setPanel(QEditorPanel panel) {
        this.panel = panel;
    }
    
      public void setFiltro(JTextField filtro) {
        this.filtro = filtro;
    }

    public void setQueues(JComboBox queues) {
        this.queue1 = queues;
    }

    public void setQueue2(JComboBox queue2) {
        this.queue2 = queue2;
    }

    public void setCheckFilter(JCheckBox checkFilter) {
        this.checkFilter = checkFilter;
    }

    public JTextField getQuantity() {
        return quantity;
    }

    public void setQuantity(JTextField quantity) {
        this.quantity = quantity;
    }
    
    
}
