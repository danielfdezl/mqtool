package biz.sos.mq.tasks;

import biz.sos.mq.app.MqTool;
import biz.sos.mq.app.panels.MovePanels;
import biz.sos.mq.pcf.WorkerPCF;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class DiscoverQMTaskMove1 extends SwingWorker<WorkerPCF, Void> {

    private MovePanels panel;

    @Override
    protected WorkerPCF doInBackground() throws Exception {
        
        WorkerPCF pcf = null;
        panel.setPcf1(null);
        try {
            pcf = new WorkerPCF(panel.getChannelsComboBox1().getSelectedItem().toString(), panel.getIp1(), panel.getPort1());

            if (pcf.getAgent() == null) {
                System.out.println("Can´t connect to " + panel.getIp1() + ", port " + panel.getPort1());
                return null;
            }else{
                panel.setPcf1(pcf);
            }
                

            List<Map<String, String>> qmParams = pcf.walkThroughQueueManagerAttributes();
            panel.setQmName1(qmParams.get(0).get("MQCA_Q_MGR_NAME"));
            panel.getjTextField3().setText(panel.getQmName1());
            panel.getjTextField3().setHorizontalAlignment(JTextField.RIGHT);

            List<String> chls = pcf.getListActiveSVRCONNChannels();
            panel.getChannelsComboBox1().removeAllItems();
            for (String chl : chls) {
                panel.getChannelsComboBox1().addItem(chl);
            }

            List<String> names = pcf.getListQueueDepth("*");
            panel.getjComboBox4().removeAllItems();
            panel.getjComboBox4().addItem("");
            boolean hideSysQ = panel.getjCheckBox1().isSelected();
            for (String name : names) {
                if(hideSysQ && name.startsWith("SYSTEM")){
                     continue;
                }else{
                    panel.getjComboBox4().addItem(name);    
                }
                
            }

            System.out.println("Ok... select a channel, a queue and work! ");

        } catch (Exception ex) {
            System.out.print(ex);
            Logger.getLogger(MqTool.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }

        return null;
    }

    public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        panel.setWorking1(false);
        panel.getjButton1().setEnabled(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }

    public void setPanel(MovePanels panel) {
        this.panel = panel;
    }
}