/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.exec.Reader;
import biz.sos.exec.Writer;
import biz.sos.mq.app.panels.QEditorPanel;
import biz.sos.mq.pcf.WorkerPCF;
import com.ibm.jms.JMSBytesMessage;
import com.ibm.jms.JMSMessage;
import com.ibm.jms.JMSTextMessage;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class PutAndGetMessageTask extends SwingWorker<Void, Void> {
    
    private QEditorPanel panel;
    private JButton button;
    private Writer writer = null;
    private Reader reader = null;
    private int iterations = 1;
    
    private JTextField filtro;
    private JComboBox queue1;
    private JComboBox queue2;
    private JCheckBox checkFilter;
    
    @Override
    protected Void doInBackground() throws Exception {
        
        panel.getjProgressBar1().setIndeterminate(true);
        button.setEnabled(false);
        panel.setWorking(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        try{
            String ip = panel.getIp();
            String port = panel.getPort();
            String qmName = panel.getQmName();
            String channel = panel.getChannelsComboBox().getSelectedItem().toString();

            String queueWrite = panel.getjComboBox6().getSelectedItem().toString();
            queueWrite = queueWrite.substring(0, queueWrite.indexOf("("));
            String queueRead = panel.getjComboBox7().getSelectedItem().toString();
            queueRead = queueRead.substring(0, queueRead.indexOf("("));

            String msg = panel.getjTextArea6().getText();
            if ("".equals(msg.trim())) {
                System.out.println("No message found.");
                return null;
            }

            writer = new Writer(ip, Integer.parseInt(port), qmName, channel, queueWrite);
            if (panel.getjCheckBox5().isSelected()) {
                writer.setUseRFH(true);
            }
            JMSTextMessage message = (JMSTextMessage) writer.getSession().createTextMessage(msg);
            writer.writeMessage(message);
            
            refreshQueueList();
            
            String msgId = message.getJMSMessageID();

            panel.getjTextArea5().removeAll();
            panel.getjCheckBox2().setSelected(false);
            
            int timeout = 0;
            try{
                 if(panel.getjTextField7().getText() != null){
                     timeout = Integer.parseInt(panel.getjTextField7().getText()) * 1000;
                 }    
            }catch(Exception e){
                timeout = 5000;
            }
           
            reader = new Reader(ip, Integer.parseInt(port), qmName, channel, queueRead, timeout);
            
            System.out.println("===> msgId: " + msgId);
            JMSMessage msgResp;
            if (panel.getjRadioButton1().isSelected()) {
                msgResp = reader.getMessage("JMSMessageID='" + msgId + "'");
            } else if (panel.getjRadioButton2().isSelected()) {
                msgResp = reader.getMessage("JMSCorrelationID='" + msgId + "'");
            } else {
                msgResp = reader.getMessage("JMSMessageID='" + msgId + "' AND JMSCorrelationID='" + msgId + "'");
            }
            
            if (msgResp != null) {

                String jmsMsg;
                if (msgResp instanceof JMSTextMessage) {
                    jmsMsg = ((JMSTextMessage) msgResp).getText();
                } else if (msgResp instanceof JMSBytesMessage) {
                    byte[] bytes = new byte[(int) ((JMSBytesMessage) msgResp).getBodyLength()];
                    ((JMSBytesMessage) msgResp).readBytes(bytes);
                    jmsMsg = new String(bytes);
                } else {
                    jmsMsg = "Mensaje con formato no soportado... sorry.";
                }
                panel.getjTextArea5().removeAll();
                panel.getjTextArea5().setText(jmsMsg);
                refreshQueueList();
                
            } else {
                panel.getjTextArea5().removeAll();
                panel.getjTextArea5().setText("<no message with MsgId or CorrelId or both>");
            }
        }finally{
            if (writer != null) {
                writer.close();
            }
            if (reader != null) {
                reader.close();
            }

        }
        
        return null;
    }

     public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        button.setEnabled(true);
        panel.setWorking(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }
     
    private void refreshQueueList(){
        WorkerPCF pcf = null;
            if(panel.getPcf() != null){
                pcf = panel.getPcf();
            }else{
                String ip = panel.getIp();
                String port = panel.getPort();
                pcf = new WorkerPCF(panel.getChannelsComboBox().getSelectedItem().toString(), ip, port);
                panel.setPcf(pcf);
            }
            
            String filter = filtro.getText();
            System.out.println("filter: " + filter);
            if ("filter...".equals(filter.trim()) || "".equals(filter.trim())) {
                filter = "*";
            }
  
            List<String> names = pcf.getListQueueDepth(filter);
            int selectedIndex1 =  queue1.getSelectedIndex();
            int selectedIndex2 = 0;
            queue1.removeAllItems();
            queue1.addItem("Queues...");
            if (queue2 != null) {
                selectedIndex2 =  queue2.getSelectedIndex();
                queue2.removeAllItems();
                queue2.addItem("Queues...");
            }
            for (String name : names) {
                if (checkFilter.isSelected() && name.startsWith("SYSTEM")) {
                    continue;
                } else {
                    queue1.addItem(name);
                    if (queue2 != null) {
                        queue2.addItem(name);
                    }

                }
            }
            queue1.setSelectedIndex(selectedIndex1);
            if (queue2 != null){
                queue2.setSelectedIndex(selectedIndex2);
            }   
    } 
     
    public void setButton(JButton button) {
        this.button = button;
    } 

    public void setPanel(QEditorPanel panel) {
        this.panel = panel;
    }
    
        public void setFiltro(JTextField filtro) {
        this.filtro = filtro;
    }

    public void setQueues(JComboBox queues) {
        this.queue1 = queues;
    }

    public void setQueue2(JComboBox queue2) {
        this.queue2 = queue2;
    }

    public void setCheckFilter(JCheckBox checkFilter) {
        this.checkFilter = checkFilter;
    }
    
    
}
