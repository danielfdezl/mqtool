/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.mq.app.panels.QEditorPanel;
import biz.sos.mq.pcf.WorkerPCF;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class RefreshQueueListTask extends SwingWorker<Void, Void> {

    private QEditorPanel panel;
    private JButton button;
    private JTextField filtro;
    private JComboBox queue1;
    private JComboBox queue2;
    private JCheckBox checkFilter;

    @Override
    protected Void doInBackground() throws Exception {
        
        panel.getjProgressBar1().setIndeterminate(true);
        button.setEnabled(false);
        panel.setWorking(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        try {
            String ip = null;
            String port = null;
            String ip_ = panel.getjTextField1().getText();
            if (ip_.contains(":")) {
                port = ip_.split(":")[1];
                ip = ip_.split(":")[0];
            } else if (ip_.contains(")")) {
                port = ip_.substring(ip_.indexOf("(") + 1, ip_.indexOf(")"));
                ip = ip_.substring(0, ip_.indexOf("("));
            } else {
                System.out.println("Set the port host to use. Ex. localohost:1414 or localhost(1414)");
                return null;
            }
            
            WorkerPCF pcf = null;
            if(panel.getPcf() != null){
                pcf = panel.getPcf();
            }else{
                pcf = new WorkerPCF(panel.getChannelsComboBox().getSelectedItem().toString(), ip, port);
                panel.setPcf(pcf);
            }
            
            //WorkerPCF pcf = new WorkerPCF(panel.getChannelsComboBox().getSelectedItem().toString(), ip, port);
            System.out.println("Filtering " + ip + ":" + port + " channel:" + panel.getChannelsComboBox().getSelectedItem());
            String filter = filtro.getText();
            
            if ("filter...".equals(filter.trim()) || "".equals(filter.trim())) {
                filter = "*";
            }
  
            List<String> names = pcf.getListQueueDepth(filter);
            int selectedIndex1 =  queue1.getSelectedIndex();
            int selectedIndex2 = 0;
            queue1.removeAllItems();
            queue1.addItem("Queues...");
            if (queue2 != null) {
                selectedIndex2 =  queue2.getSelectedIndex();
                queue2.removeAllItems();
                queue2.addItem("Queues...");
            }
            for (String name : names) {
                if (checkFilter.isSelected() && name.startsWith("SYSTEM")) {
                    continue;
                } else {
                    if(checkFilter.isSelected() && !name.startsWith("AMQ")){
                        queue1.addItem(name);
                    }else if(!checkFilter.isSelected()){
                         queue1.addItem(name);
                    }
                    if (queue2 != null) {
                        queue2.addItem(name);
                    }
                }
                
            }
            queue1.setSelectedIndex(selectedIndex1);
            if (queue2 != null){
                queue2.setSelectedIndex(selectedIndex2);
            }    
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        button.setEnabled(true);
        panel.setWorking(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }

    public void setPanel(QEditorPanel panel) {
        this.panel = panel;
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    public void setFiltro(JTextField filtro) {
        this.filtro = filtro;
    }

    public void setQueues(JComboBox queues) {
        this.queue1 = queues;
    }

    public void setQueue2(JComboBox queue2) {
        this.queue2 = queue2;
    }

    public void setCheckFilter(JCheckBox checkFilter) {
        this.checkFilter = checkFilter;
    }
}