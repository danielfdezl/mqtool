/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.exec.Reader;
import biz.sos.exec.Writer;
import biz.sos.mq.app.panels.TestingPanels;
import biz.sos.swing.util.StringOperations;
import com.ibm.jms.JMSBytesMessage;
import com.ibm.jms.JMSMessage;
import com.ibm.jms.JMSTextMessage;
import com.ibm.msg.client.jms.JmsConstants;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.swing.JButton;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class PutAndGetMsgIterationsTask extends SwingWorker<Void, Void> {

    private TestingPanels panel;
    private JButton button;
    private Writer writer = null;
    private Reader reader = null;
    private int iterations = 1;
    private int messagesPerIterations = 1;
    private int timeBetweenIterations = 5;
    private int timeout = 5;
    private List <Map<String, Object>> ids = null;

    @Override
    protected Void doInBackground() throws Exception {
        
        int counter = 0;
        int interationCounter = 0;
        try {
            String ip = panel.getIp();
            String port = panel.getPort();
            String qmName = panel.getQmName();
            String channel = panel.getChannelsComboBox().getSelectedItem().toString();
            boolean withLogs = panel.getjCheckBox6().isSelected();

            long sec1 = Long.parseLong(panel.getjTextField5().getText());
            int sec1Step = Integer.parseInt(panel.getjTextField7().getText());
            long sec1Max = Long.parseLong(panel.getjTextField6().getText());
            int sec1Len = String.valueOf(sec1Max).length();
            boolean fixLen = panel.getjCheckBox1().isSelected();
            
            int sec2Len = Integer.parseInt(panel.getjTextField16().getText());
            int sec3Len = Integer.parseInt(panel.getjTextField19().getText());
          
            String tokenSec4 =   panel.getjTextField11().getText();
            String tokenSec5 =   panel.getjTextField15().getText();
            SimpleDateFormat sdfSec4 = null;
            SimpleDateFormat sdfSec5 = null;
            boolean sec4Ok = true;
            boolean sec5Ok = true;
            try{
                 sdfSec4 = new SimpleDateFormat(tokenSec4);
            }catch(Exception e){
                System.out.println("Token 4 present some error, check it please.");
                sec4Ok = false;
            }
            try{
                 sdfSec5 = new SimpleDateFormat(tokenSec5);
            }catch(Exception e){
                System.out.println("Token 5 present some error, check it please.");
                sec5Ok = false;
            }
            
            SimpleDateFormat sdf =  new SimpleDateFormat("yyyyMMdd HHmmssSSS");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            
            String queueWrite = panel.getjComboBox1().getSelectedItem().toString();
            queueWrite = queueWrite.substring(0, queueWrite.indexOf("("));
            String queueRead = panel.getjComboBox3().getSelectedItem().toString();
            try{
                queueRead = queueRead.substring(0, queueRead.indexOf("("));
            }catch(Exception e){
                queueRead = "";
            }    
            String msg = panel.getjTextArea1().getText();
            if ("".equals(msg.trim())) {
                System.out.println("No message found.");
                return null;
            }

            if (panel.getjCheckBox5().isSelected()) {
                writer.setUseRFH(true);
            }
            boolean replaceCarrFeed = false;
            if (panel.getjCheckBox7().isSelected()) {
                replaceCarrFeed = true;
            }

            writer = new Writer(ip, Integer.parseInt(port), qmName, channel, queueWrite);
            
            if(panel.getjCheckBox3().isSelected()){
                reader = new Reader(ip, Integer.parseInt(port), qmName, channel, queueRead, timeout*1000);
            }
            for (int iteration = 0; iteration < iterations; iteration++) {
                
                interationCounter = iteration;
                
                ids = new ArrayList <Map<String, Object>>();
                long sec1Current = sec1;
                for(int messageCount = 0; messageCount < messagesPerIterations; messageCount++){
                
                    String sec1Str =  String.valueOf(sec1Current);
                    if(fixLen){
                       sec1Str = StringOperations.refillLeftCero(sec1Len,sec1Str); 
                    }
                    
                    String currentMsg = msg.replaceAll("\\{sec1\\}",sec1Str);
                    currentMsg = currentMsg.replaceAll("\\{sec2\\}", StringOperations.alphanumericRandom(sec2Len));
                    currentMsg = currentMsg.replaceAll("\\{sec3\\}", StringOperations.numericRandom(sec3Len));     
                    if(sec4Ok){
                        currentMsg = currentMsg.replaceAll("\\{sec4\\}",sdfSec4.format(new Date()));
                    }    
                    if(sec5Ok){
                        currentMsg = currentMsg.replaceAll("\\{sec5\\}",sdfSec5.format(new Date()));
                    }
                    
                    if(replaceCarrFeed){
                         currentMsg = currentMsg.replaceAll("\n", "\r\n");
                    }
                    JMSTextMessage message = (JMSTextMessage) writer.getSession().createTextMessage(currentMsg);
                    Map <String, Object> m = new HashMap<String, Object>();
                    writer.writeMessage(message);
                    counter = messageCount;
                    String msgId = message.getJMSMessageID();
                    m.put("msgId", msgId);
                    m.put("putTime", message.getJMSTimestamp());
                    
                    ids.add(m);
                    if(withLogs)
                        System.out.println( (messageCount+1) + ": put msg [" + currentMsg + "] with id: "  +  msgId );
                    
                    sec1Current  = sec1Current + sec1Step;
                    if(sec1Current>sec1Max){
                        sec1Current = sec1;
                    }
                    
                }     
                 
                if (panel.getjCheckBox3().isSelected()) { //wait for responses
                    
                    for(int messageCount = 0; messageCount < messagesPerIterations; messageCount++){ 
                        String msgId = (String)ids.get(messageCount).get("msgId");
                        if(withLogs)
                            System.out.println("===> get by msgId: " + msgId);
                        JMSMessage msgResp;
                        if (panel.getjRadioButton1().isSelected()) {
                            msgResp = reader.getMessage("JMSMessageID='" + msgId + "'");
                        } else if (panel.getjRadioButton2().isSelected()) {
                            msgResp = reader.getMessage("JMSCorrelationID='" + msgId + "'");
                        } else {
                            msgResp = reader.getMessage("JMSMessageID='" + msgId + "' AND JMSCorrelationID='" + msgId + "'");
                        }

                        if (msgResp != null) {
                            System.out.println("==================>" + msgId + "\n");
                            
                            Enumeration enu = msgResp.getPropertyNames();
                            while(enu.hasMoreElements()){
                                String name = (String)enu.nextElement();
                                System.out.println(name + ": " + msgResp.getStringProperty(name));
                            }
                             
                            
                            String str2 =  msgResp.getStringProperty("JMS_IBM_PutDate") + " " +  msgResp.getStringProperty(JmsConstants.JMS_IBM_PUTTIME) + "0";
                            
                            System.out.println("-22222 date     : " + sdf.format(new Date()) + "\n");
                            System.out.println("-22222 JMS_IBM_PutTime : " + msgResp.getStringProperty("JMS_IBM_PutTime") + "\n");
                            System.out.println("-22222 JMS_IBM_PutDate : " + msgResp.getStringProperty("JMS_IBM_PutDate") + "\n");
                            
                            long t1 = (Long)ids.get(messageCount).get("putTime");
                            long t2 =  sdf.parse(str2).getTime() ;
                            
                            System.out.println("\n t1: " + t1); 
                            System.out.println("\n t2: " + t2); 
                            System.out.println("\n Diff: " + (t1-t2));
                            
                            String jmsMsg;
                            if (msgResp instanceof JMSTextMessage) {
                                jmsMsg = ((JMSTextMessage) msgResp).getText();
                            } else if (msgResp instanceof JMSBytesMessage) {
                                byte[] bytes = new byte[(int) ((JMSBytesMessage) msgResp).getBodyLength()];
                                ((JMSBytesMessage) msgResp).readBytes(bytes);
                                jmsMsg = new String(bytes);
                            } else {
                                jmsMsg = "Mensaje con formato no soportado... sorry.";
                            }
                            if(withLogs) 
                                System.out.println(" message retrieved: " + msgId);

                        } else {
                             if(withLogs)   
                                System.out.println(" No message retrieved, msgIg: " + msgId);
                        }

                    }
                 }   

                if (iteration+1 < iterations) {
                    System.out.println("Iteration " + (iteration+1) + " finalized, waiting " + timeBetweenIterations + " seconds.");
                    Thread.sleep(timeBetweenIterations * 1000);
                } else {
                    System.out.println("End.");
                }
            }



        } catch(Exception e){
            //e.printStackTrace();
            
            System.out.println("#### Ocurrió un error ... " + e.getMessage());
            System.out.println("#### Iteración número " + interationCounter + "");
            System.out.println("#### Se inyectaron " + (counter+1) + " mensajes.");
            System.out.println("####");
            
        }finally {
            if (writer != null) {
                writer.close();
            }
            if (reader != null) {
                reader.close();
            }

        }

        return null;
    }

    public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        button.setEnabled(true);
        panel.setWorking(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    public void setPanel(TestingPanels panel) {
        this.panel = panel;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public void setMessagesPerIterations(int messagesPerIterations) {
        this.messagesPerIterations = messagesPerIterations;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setTimeBetweenIterations(int timeBetweenIterations) {
        this.timeBetweenIterations = timeBetweenIterations;
    }


}
