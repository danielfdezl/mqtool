/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.exec.Writer;
import biz.sos.mq.app.panels.BrowseTable;
import biz.sos.mq.app.panels.MovePanels;
import biz.sos.swing.util.CheckBoxHeader;
import biz.sos.xml.XmlFormatter;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.table.TableColumn;

/**
 *
 * @author Daniel Fdez
 */
public class BrowseMessageTaskMove1 extends SwingWorker<Void, Void> {
    
    private MovePanels panel;
    private Writer writer = null;
    private javax.swing.JDesktopPane jDesktopPane1;
     
    @Override
    protected Void doInBackground()  {
        
        try {
             String ip = panel.getIp1();
             String port = panel.getPort1();
             String qmName = panel.getQmName1();
             String channel = panel.getChannelsComboBox1().getSelectedItem().toString();
             String queue = panel.getjComboBox4().getSelectedItem().toString();
             if(queue != null && queue.indexOf("(") != -1){
                 queue = queue.substring(0, queue.indexOf("("));
             }else{
                 System.out.println("No queue...");
                 return null;
               }
             

             final JTable table = BrowseTable.getTable(ip, Integer.parseInt(port), qmName, channel, queue);

             if(table != null){  

                 JScrollPane browseScrollPane = new javax.swing.JScrollPane(table);
                 int w = panel.getjScrollPane1().getWidth();
                 int h =  panel.getjScrollPane1().getHeight();
                 browseScrollPane.setSize(w,h);

                 panel.getjPanel5().removeAll();
                 panel.getjPanel5().doLayout();
                 panel.getjPanel5().add(browseScrollPane);
                 panel.getjPanel5().doLayout();

                 table.setRowSelectionAllowed(true);
                 table.setCellSelectionEnabled(true);
                 table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
                 ListSelectionModel cellSelectionModel = table.getSelectionModel();
                 cellSelectionModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

                 TableColumn tc = table.getColumnModel().getColumn(0);
                 tc.setCellEditor(table.getDefaultEditor(Boolean.class));
                 tc.setCellRenderer(table.getDefaultRenderer(Boolean.class));

                 MyItemListener lis = new MyItemListener();
                 lis.setTable(table);
                 tc.setHeaderRenderer(new CheckBoxHeader(lis));

                 table.addMouseListener(new java.awt.event.MouseAdapter() {
                     @Override
                     public void mousePressed(java.awt.event.MouseEvent evt) {  
                         int[] selectedRow = table.getSelectedRows();
                         int[] selectedColumns = table.getSelectedColumns();
                         if(selectedColumns[0] > 0){
                             Object o = table.getValueAt(table.getSelectedRow(), 2);  
                             String s = o.toString();              
                             ver(s);
                         }else{
                             Object o = table.getValueAt(selectedRow[0], 0);
                             if((Boolean)o){
                                 table.setValueAt(false, selectedRow[0], 0);
                             }else{
                                 table.setValueAt(true, selectedRow[0], 0);
                             }
                         }
                     }
                 });

             }
        } catch (Exception e) {
            System.out.println("BrowseMessageTaskMove1 ..." + e);
            e.printStackTrace();
        }
        return null;
    }

    	class MyItemListener implements ItemListener {
            JTable table;
            public void itemStateChanged(ItemEvent e) {
                    Object source = e.getSource();
                    if (source instanceof AbstractButton == false)
                            return;
                    boolean checked = e.getStateChange() == ItemEvent.SELECTED;
                    for (int x = 0, y = table.getRowCount(); x < y; x++) {
                            table.setValueAt(new Boolean(checked), x, 0);
                    }
            }
            public void setTable(JTable table){
                this.table = table;
            }
	}
    
    
     public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
     //   button.setEnabled(true);
     //   panel.setWorking(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }
     
    public void setButton(JButton button) {
     //   this.button = button;
    } 

    public void setPanel(MovePanels panel) {
        this.panel = panel;
    }
    
    public void setJDesktopPanel(JDesktopPane jDesktopPane1) {
        this.jDesktopPane1 = jDesktopPane1;
    }
    
    private void ver(String msg) {                                         
        if ("".equals(msg.trim())) {
            System.out.println("<no message>");
            return;
        }
        JInternalFrame frame = new javax.swing.JInternalFrame();
        JScrollPane jScrollPane = new javax.swing.JScrollPane();
        
        JTextArea textArea = new JTextArea();
        jScrollPane.setViewportView(textArea);
        
        frame.getContentPane().add(jScrollPane);
        frame.setSize(800, 600);
        frame.setClosable(true);
        frame.setResizable(true);
        frame.setIconifiable(true);
        

        String xmlFormatted = XmlFormatter.prettyFormat2(msg);
        if (xmlFormatted != null) {
            textArea.setText(xmlFormatted);
        } else {
            textArea.setText(msg);
        }

        jDesktopPane1.add(frame, javax.swing.JLayeredPane.DEFAULT_LAYER);
        frame.setVisible(true);

    }
}
