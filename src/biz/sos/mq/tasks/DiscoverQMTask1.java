package biz.sos.mq.tasks;

import biz.sos.mq.app.MqTool;
import biz.sos.mq.app.panels.TestingPanels;
import biz.sos.mq.pcf.WorkerPCF;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class DiscoverQMTask1 extends SwingWorker<WorkerPCF, Void> {

    private TestingPanels panel;

    @Override
    protected WorkerPCF doInBackground() throws Exception {
        
        WorkerPCF pcf = null;
        
        try {
            pcf = new WorkerPCF(panel.getChannelsComboBox().getSelectedItem().toString(), panel.getIp(), panel.getPort());

            if (pcf.getAgent() == null) {
                System.out.println("Can´t connect to " + panel.getIp() + ", port " + panel.getPort());
                return null;
            }else{
                panel.setPcf(pcf);
            }

            List<Map<String, String>> qmParams = pcf.walkThroughQueueManagerAttributes();
            panel.setQmName(qmParams.get(0).get("MQCA_Q_MGR_NAME"));
            panel.getjTextField3().setText(panel.getQmName());
            panel.getjTextField3().setHorizontalAlignment(JTextField.RIGHT);

            List<String> chls = pcf.getListActiveSVRCONNChannels();
            panel.getChannelsComboBox().removeAllItems();
            for (String chl : chls) {
                panel.getChannelsComboBox().addItem(chl);
            }

            List<String> names = pcf.getListQueueDepth("*");
            panel.getjComboBox1().removeAllItems();
            panel.getjComboBox3().removeAllItems();
            panel.getjComboBox1().addItem("");
            panel.getjComboBox3().addItem("");   
            boolean hideSysQ = panel.getjCheckBox2().isSelected();
            for (String name : names) {
                if(hideSysQ && name.startsWith("SYSTEM")){
                     continue;
                }else{
                    
                    if(hideSysQ && name.startsWith("AMQ")){
                        continue;
                    }else{
                        panel.getjComboBox1().addItem(name);
                        panel.getjComboBox3().addItem(name);    
                    }
                    
                }
                
            }

            System.out.println("Ok... select a channel, a queue and work! ");
            panel.getjButton1().setEnabled(true);

        } catch (Exception ex) {
            System.out.print(ex);
            Logger.getLogger(MqTool.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }

        return null;
    }

    public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        panel.setWorking(false);
        panel.getjButton1().setEnabled(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }

    public void setPanel(TestingPanels panel) {
        this.panel = panel;
    }
}