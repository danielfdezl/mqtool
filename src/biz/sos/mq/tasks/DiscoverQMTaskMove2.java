package biz.sos.mq.tasks;

import biz.sos.mq.app.MqTool;
import biz.sos.mq.app.panels.MovePanels;
import biz.sos.mq.pcf.WorkerPCF;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class DiscoverQMTaskMove2 extends SwingWorker<WorkerPCF, Void> {

    private MovePanels panel;

    @Override
    protected WorkerPCF doInBackground() throws Exception {
        
        WorkerPCF pcf = null;
        
       panel.setPcf2(null);
        
        try {
            pcf = new WorkerPCF(panel.getChannelsComboBox2().getSelectedItem().toString(), panel.getIp2(), panel.getPort2());

            if (pcf.getAgent() == null) {
                System.out.println("Can´t connect to " + panel.getIp2() + ", port2 " + panel.getPort2());
                return null;
            }else{
                panel.setPcf2(pcf);
            }

            List<Map<String, String>> qmParams = pcf.walkThroughQueueManagerAttributes();
            panel.setQmName2(qmParams.get(0).get("MQCA_Q_MGR_NAME"));
            panel.getjTextField4().setText(panel.getQmName2());
            panel.getjTextField4().setHorizontalAlignment(JTextField.RIGHT);

            List<String> chls = pcf.getListActiveSVRCONNChannels();
            panel.getChannelsComboBox2().removeAllItems();
            for (String chl : chls) {
                panel.getChannelsComboBox2().addItem(chl);
            }

        List<String> names = pcf.getListQueueDepthOrdered("*");

        DefaultListModel modelo = new DefaultListModel();
        for (String name : names) {
            if (panel.getjCheckBox2().isSelected() && name.startsWith("SYSTEM")) {
                continue;
            } else {
                if(panel.getjCheckBox2().isSelected() && !name.startsWith("AMQ.")){
                   modelo.addElement(name);
                }else if(!panel.getjCheckBox2().isSelected()){
                   modelo.addElement(name); 
                }
            }
        }
        panel.getjList1().setModel(modelo);
        } catch (Exception ex) {
            System.out.print(ex);
            Logger.getLogger(MqTool.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }

        return null;
    }

    public void done() { 
        panel.getjProgressBar1().setIndeterminate(false);
        panel.setWorking2(false);
        panel.getjButton2().setEnabled(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }

    public void setPanel(MovePanels panel) {
        this.panel = panel;
    }
}