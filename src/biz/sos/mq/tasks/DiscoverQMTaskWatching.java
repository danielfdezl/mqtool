package biz.sos.mq.tasks;

import biz.sos.mq.app.MqTool;
import biz.sos.mq.app.panels.QEditorPanel;
import biz.sos.mq.app.panels.WatchingPanel;
import biz.sos.mq.pcf.WorkerPCF;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class DiscoverQMTaskWatching extends SwingWorker<WorkerPCF, Void> {

    private WatchingPanel panel;

    @Override
    protected WorkerPCF doInBackground() throws Exception {
        
        panel.getjProgressBar1().setIndeterminate(true);
        panel.setWorking(true);
        panel.getjButton1().setEnabled(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        WorkerPCF pcf = null;
        
        try {
            pcf = new WorkerPCF(panel.getChannelsComboBox().getSelectedItem().toString(), panel.getIp(), panel.getPort());
            panel.setPcf(pcf);
            if (pcf.getAgent() == null) {
                System.out.println("Can´t connect to " + panel.getIp() + ", port " + panel.getPort());
                return null;
            }

            List<Map<String, String>> qmParams = pcf.walkThroughQueueManagerAttributes();
            panel.setQmName(qmParams.get(0).get("MQCA_Q_MGR_NAME"));
            panel.getjTextField3().setText(panel.getQmName());
            panel.getjTextField3().setHorizontalAlignment(JTextField.RIGHT);

            List<String> chls = pcf.getListActiveSVRCONNChannels();
            String  selected = panel.getChannelsComboBox().getSelectedItem().toString();
            panel.getChannelsComboBox().removeAllItems();
            int index = 0;
            int i = 0;
            for (String chl : chls) {
                
                panel.getChannelsComboBox().addItem(chl);
                if(chl.equals(selected) ){
                     index = i;
                }
                i++;
            }
            panel.getChannelsComboBox().setSelectedIndex(index);
                    
            List<String> names = pcf.getListQueueDepth("*");
            for (String name : names) {
                
                 if (name.startsWith("SYSTEM")) {
                    continue;
                } else {
                }
            }
            

            System.out.println("Ok... select a channel, a queue and work! ");

        } catch (Exception ex) {
            System.out.print(ex);
            Logger.getLogger(MqTool.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }

        return null;
    }

    public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        panel.setWorking(false);
        panel.getjButton1().setEnabled(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
        
    }

    public void setPanel(WatchingPanel panel) {
        this.panel = panel;
    }
}