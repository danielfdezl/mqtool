/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.exec.Writer;
import biz.sos.mq.app.panels.BrowseTable;
import biz.sos.mq.app.panels.QEditorPanel;
import biz.sos.xml.XmlFormatter;
import java.awt.Cursor;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class BrowseMessageTask extends SwingWorker<Void, Void> {
    
    private QEditorPanel panel;
    private JButton button;
    private Writer writer = null;
    private javax.swing.JDesktopPane jDesktopPane1;
     
    @Override
    protected Void doInBackground() throws Exception {
        
        button.setEnabled(false);
        panel.setWorking(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        String ip = panel.getIp();
        String port = panel.getPort();
        String qmName = panel.getQmName();
        String channel = panel.getChannelsComboBox().getSelectedItem().toString();
        String queue = panel.getjComboBox3().getSelectedItem().toString();
        queue = queue.substring(0, queue.indexOf("("));

        final JTable table = BrowseTable.getTable(ip, Integer.parseInt(port), qmName, channel, queue);

        if(table != null){
            JScrollPane browseScrollPane = new javax.swing.JScrollPane(table);
            panel.getjPanel1().removeAll();
            panel.getjPanel1().add(browseScrollPane);
            panel.getjPanel1().doLayout();
      /*      
            table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
                public void valueChanged(ListSelectionEvent event) {
                    Object o = table.getValueAt(table.getSelectedRow(), 2);  
                    String s = o.toString();              
                    ver(s);
                }
            });  
     */       
            table.addMouseListener(new java.awt.event.MouseAdapter() {
                     @Override
                     public void mousePressed(java.awt.event.MouseEvent evt) {  
                         int[] selectedRow = table.getSelectedRows();
                         int[] selectedColumns = table.getSelectedColumns();
                         if(selectedColumns[0] > 0){
                             Object o = table.getValueAt(table.getSelectedRow(), 2);  
                             String s = o.toString();              
                             ver(s);
                         }else{
                             Object o = table.getValueAt(selectedRow[0], 0);
                             if((Boolean)o){
                                 table.setValueAt(false, selectedRow[0], 0);
                             }else{
                                 table.setValueAt(true, selectedRow[0], 0);
                             }
                         }
                     }
                 });
            
        }
        return null;
    }

     public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        button.setEnabled(true);
        panel.setWorking(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }
     
    public void setButton(JButton button) {
        this.button = button;
    } 

    public void setPanel(QEditorPanel panel) {
        this.panel = panel;
    }
    
    public void setJDesktopPanel(JDesktopPane jDesktopPane1) {
        this.jDesktopPane1 = jDesktopPane1;
    }
    
    private void ver(String msg) {                                         
        if ("".equals(msg.trim())) {
            System.out.println("<no message>");
            return;
        }
        JInternalFrame frame = new javax.swing.JInternalFrame();
        JScrollPane jScrollPane = new javax.swing.JScrollPane();
        
        JTextArea textArea = new JTextArea();
        jScrollPane.setViewportView(textArea);
        
        frame.getContentPane().add(jScrollPane);
        frame.setSize(800, 600);
        frame.setClosable(true);
        frame.setResizable(true);
        frame.setIconifiable(true);
        

        String xmlFormatted = XmlFormatter.prettyFormat2(msg);
        if (xmlFormatted != null) {
            textArea.setText(xmlFormatted);
        } else {
            textArea.setText(msg);
        }

        jDesktopPane1.add(frame, javax.swing.JLayeredPane.DEFAULT_LAYER);
        frame.setVisible(true);

    }
}
