/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.mq.tasks;

import biz.sos.exec.Reader;
import biz.sos.mq.app.panels.QEditorPanel;
import biz.sos.mq.pcf.WorkerPCF;
import com.ibm.jms.JMSBytesMessage;
import com.ibm.jms.JMSMessage;
import com.ibm.jms.JMSTextMessage;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 *
 * @author Daniel Fdez
 */
public class GetMessageTask extends SwingWorker<Void, Void> {

    private QEditorPanel panel;
    private JButton button;
    private Reader reader = null;
    
    private JTextField filtro;
    private JComboBox queue1;
    private JComboBox queue2;
    private JCheckBox checkFilter;

    @Override
    protected Void doInBackground() throws Exception {

        panel.getjProgressBar1().setIndeterminate(true);
        button.setEnabled(false);
        panel.setWorking(true);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        String ip = panel.getIp();
        String port = panel.getPort();
        String qmName = panel.getQmName();
        String channel = panel.getChannelsComboBox().getSelectedItem().toString();
        String queue = panel.getjComboBox4().getSelectedItem().toString();
        queue = queue.substring(0, queue.indexOf("("));

        reader = new Reader(ip, Integer.parseInt(port), qmName, channel, queue, 2);

        System.out.println("listening  on " + qmName + " " + queue + " !!!");

        if (!panel.getjCheckBox1().isSelected()) {
            JMSMessage jmsMsg = reader.getMessage();
            
            System.out.println("===>" + jmsMsg);
            
            String msg;
            if (jmsMsg != null) {
                if (jmsMsg instanceof JMSTextMessage) {
                    msg = ((JMSTextMessage) jmsMsg).getText();
                } else if (jmsMsg instanceof JMSBytesMessage) {
                    byte[] bytes = new byte[(int) ((JMSBytesMessage) jmsMsg).getBodyLength()];
                    ((JMSBytesMessage) jmsMsg).readBytes(bytes);
                    msg = new String(bytes);
                } else {
                    msg = "Mensaje con formato no soportado... sorry.";
                }
                panel.getjTextArea4().removeAll();
                panel.getjTextArea4().setText(msg);
                panel.setAutoscrolls(true);
                System.out.println("===> msgId: " + jmsMsg.getJMSMessageID());
            }

        } else {
            panel.getjTextArea4().removeAll();
            boolean go = true;
            int i = 0;
            while (go) {
                JMSMessage jmsMsg = reader.getMessage();
                String msg;
                if (jmsMsg != null) {
                    if (jmsMsg instanceof JMSTextMessage) {
                        msg = ((JMSTextMessage) jmsMsg).getText();
                    } else if (jmsMsg instanceof JMSBytesMessage) {
                        byte[] bytes = new byte[(int) ((JMSBytesMessage) jmsMsg).getBodyLength()];
                        ((JMSBytesMessage) jmsMsg).readBytes(bytes);
                        msg = new String(bytes);
                    } else {
                        msg = "Mensaje con formato no soportado... sorry.";
                    }

                    panel.getjTextArea4().append(((i > 0) ? "\n" : "") + ++i + " =====================================\n");
                    panel.getjTextArea4().append(msg);
                    System.out.println(jmsMsg.getJMSMessageID());
                } else {
                    go = false;
                }
            }
        }
        reader.close();
        System.out.println("finish!");

        refreshQueueList();    

        return null;
    }

    public void done() {
        panel.getjProgressBar1().setIndeterminate(false);
        button.setEnabled(true);
        panel.setWorking(false);
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        Toolkit.getDefaultToolkit().beep();
    }

    private void refreshQueueList(){
        
        WorkerPCF pcf = null;
        String ip = panel.getIp();
        String port = panel.getPort();
        if(panel.getPcf() != null){
            pcf = panel.getPcf();
        }else{
            pcf = new WorkerPCF(panel.getChannelsComboBox().getSelectedItem().toString(), ip, port);
            panel.setPcf(pcf);
        }

        String filter = filtro.getText();
        if ("filter...".equals(filter.trim()) || "".equals(filter.trim())) {
            filter = "*";
        }

        List<String> names = pcf.getListQueueDepth(filter);
        int selectedIndex1 =  queue1.getSelectedIndex();
        int selectedIndex2 = 0;
        queue1.removeAllItems();
        queue1.addItem("Queues...");
        if (queue2 != null) {
            selectedIndex2 =  queue2.getSelectedIndex();
            queue2.removeAllItems();
            queue2.addItem("Queues...");
        }
        for (String name : names) {
           
             if (checkFilter.isSelected() && name.startsWith("SYSTEM")) {
                    continue;
                } else {
                    if(checkFilter.isSelected() && !name.startsWith("AMQ")){
                        queue1.addItem(name);
                    }else if(!checkFilter.isSelected()){
                         queue1.addItem(name);
                    }
                    if (queue2 != null) {
                        queue2.addItem(name);
                    }
                }
        }
        queue1.setSelectedIndex(selectedIndex1);
        if (queue2 != null){
            queue2.setSelectedIndex(selectedIndex2);
        }  
        
    }
    
    public void setButton(JButton button) {
        this.button = button;
    }

    public void setPanel(QEditorPanel panel) {
        this.panel = panel;
    }
    
      public void setFiltro(JTextField filtro) {
        this.filtro = filtro;
    }

    public void setQueues(JComboBox queues) {
        this.queue1 = queues;
    }

    public void setQueue2(JComboBox queue2) {
        this.queue2 = queue2;
    }

    public void setCheckFilter(JCheckBox checkFilter) {
        this.checkFilter = checkFilter;
    }
}
