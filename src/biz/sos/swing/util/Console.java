/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.swing.util;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.Timer;

public class Console implements Runnable {

    JTextArea displayPane;
    BufferedReader reader;

     public Console() {
        try {
            PipedOutputStream pos = new PipedOutputStream();
            System.setOut(new PrintStream(pos, true));
            System.setErr(new PrintStream(pos, true));
            PipedInputStream pis = new PipedInputStream(pos);
            reader = new BufferedReader(new InputStreamReader(pis));
        } catch (IOException e) {
        }
    }

    public void run() {
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                displayPane.append(line + "\n");
                displayPane.setCaretPosition(displayPane.getDocument().getLength());
            }
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,
                    "Error redirecting output : " + ioe.getMessage());
        }
    }

    public void redirectOutput(JTextArea displayPane) {
        this.displayPane = displayPane;
        this.redirectOut();
        this.redirectErr();
    }

    public void redirectOut() {
        new Thread(this).start();
    }

    public void redirectErr() {
        new Thread(this).start();
    }

    public static void main(String[] args) {

        JTextArea textArea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textArea);

        JFrame frame = new JFrame("Redirect Output");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(scrollPane);
        frame.setSize(800, 600);
        frame.setVisible(true);

        Console console = new Console();
        console.redirectOutput(textArea);
        final int i = 0;

        Timer timer = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("System.out ... date: " + new java.util.Date().toString());
                System.err.println("System.err ... currentTimeMillis: " + System.currentTimeMillis());
            }
        });
        timer.start();

    }
}
