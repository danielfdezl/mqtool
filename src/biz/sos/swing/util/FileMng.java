/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.swing.util;

import biz.sos.mq.app.panels.PutAndGet;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;

/**
 *
 * @author Daniel Fdez
 */
public class FileMng {
    
    private final static String confFileName= "connections.properties";
    
    public static void loadConnections(JComboBox jComboBox4) {
        BufferedReader br = null;
        try {
            File f = new File(confFileName);
            if(!f.exists() ){
                return;
            }
            br = new BufferedReader(new FileReader(f));
            String line = br.readLine();
            jComboBox4.removeAllItems();
            while (line != null) {
                String [] arr = line.split(";");
                jComboBox4.addItem(arr[0]);
                line = br.readLine();
            }
        }catch (FileNotFoundException ex) {
            Logger.getLogger(PutAndGet.class.getName()).log(Level.INFO, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(PutAndGet.class.getName()).log(Level.INFO, null, ex);
        }catch (Exception ex) {

        } finally {
            try {
                if(br != null)
                    br.close();
            } catch (IOException ex) {
                Logger.getLogger(PutAndGet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
     public static void updateFile(String str){
        
        File tmp = null;
        String [] arr = str.split(";");
        try {
            
            File f = new File(confFileName);
            if (!f.exists()) {
                f.createNewFile();
            }
            
            tmp = File.createTempFile("tmp", "");
            BufferedReader br = new BufferedReader(new FileReader(new File(confFileName)));
            BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));

            String l = null;
            while( (l = br.readLine()) != null){
                if(!l.startsWith(arr[0])){
                    bw.write(String.format("%s%n", l));
                }
            }
            bw.write(String.format("%s%n", str));
            
            br.close();
            bw.close();

            File oldFile = new File(confFileName);
            if (oldFile.delete()) {
                tmp.renameTo(oldFile);
            }
            
        } catch (IOException ex) {
        }
    }
     
      public static void delete(String str){
        
        File tmp = null;
        try {
            
            File f = new File(confFileName);
            if (!f.exists()) {
                f.createNewFile();
            }
            
            tmp = File.createTempFile("tmp", "");
            BufferedReader br = new BufferedReader(new FileReader(new File(confFileName)));
            BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));

            String l = null;
            while( (l = br.readLine()) != null){
                if(!l.startsWith(str+";")){
                    bw.write(String.format("%s%n", l));
                }
            }
            
            br.close();
            bw.close();

            File oldFile = new File(confFileName);
            if (oldFile.delete()) {
                tmp.renameTo(oldFile);
            }
            
        } catch (IOException ex) {
        }
    }
     
     public static String [] search(String connectionName){
        BufferedReader br = null;
        String line = null;
        try {
            File f = new File(confFileName);
            if(!f.exists() ){
                return null;
            }
            br = new BufferedReader(new FileReader(f));
            line = br.readLine();
            while (line != null) {
                String [] arr = line.split(";");
                if(arr[0].equals(connectionName)){
                  break;
                }
                line = br.readLine();
            }
        }catch (FileNotFoundException ex) {
            Logger.getLogger(PutAndGet.class.getName()).log(Level.INFO, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(PutAndGet.class.getName()).log(Level.INFO, null, ex);
        }catch (Exception ex) {
             System.out.println(ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(PutAndGet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(line != null)
            return line.split(";");
        else
            return null;
     }
    
}
