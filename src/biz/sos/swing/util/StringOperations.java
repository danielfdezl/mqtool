/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.swing.util;

import java.util.Random;

/**
 *
 * @author Daniel Fdez
 */
public class StringOperations {
    
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static final String NUM = "0123456789";
    static Random rnd = new Random();
    
    public static String refillLeftCero(int len, String value) {
        while (value.length() < len) {
            value = "0" + value;
        }
        return value;
    }

    public static String alphanumericRandom(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
    
     public static String numericRandom(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(NUM.charAt(rnd.nextInt(NUM.length())));
        }
        return sb.toString();
    }
    
}
