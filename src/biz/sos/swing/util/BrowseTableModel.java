/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.sos.swing.util;

import java.util.List;
import java.util.Map;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Daniel Fdez
 */
public class BrowseTableModel extends AbstractTableModel {

    public BrowseTableModel(List<Map<String, String>> l) {
        
        rowData = new Object[l.size()][3];
        int row = 0;
        for (Map<String, String> m : l) {
            int i = 0;
            rowData[row][0] =  Boolean.FALSE;
            rowData[row][1] = m.get("JMS_IBM_PutDate");
            rowData[row][2] = m.get("body");
            row++;
        }
    }

  Object rowData[][];
  //= { { "1", Boolean.TRUE }, { "2", Boolean.TRUE }, { "3", Boolean.FALSE },
  //    { "4", Boolean.TRUE }, { "5", Boolean.FALSE }, };

    public Object[][] getRowData() {
        return rowData;
    }

    public void setRowData(Object[][] rowData) {
        this.rowData = rowData;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }
  
  String columnNames[] = { "", "Date","Body" };

  public int getColumnCount() {
    return columnNames.length;
  }

  public String getColumnName(int column) {
    return columnNames[column];
  }

  public int getRowCount() {
    return rowData.length;
  }

  public Object getValueAt(int row, int column) {
    return rowData[row][column];
  }

  public Class getColumnClass(int column) {
    return (getValueAt(0, column).getClass());
  }

  public void setValueAt(Object value, int row, int column) {
    rowData[row][column] = value;
  }

  public boolean isCellEditable(int row, int column) {
    return (column != 0);
  }
}